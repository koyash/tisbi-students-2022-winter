﻿using System;
using System.Net.Http;
using Refit;

namespace TestApp.OnlineServices
{
    public class BaseDataService<T>
    {

        public T InstanceInterface
        {
            get
            {
                var client = new HttpClient()
                {
                    BaseAddress = new Uri(AppSettings.BaseURI)
                };
                return RestService.For<T>(client);
            }
        }
        
    }
}