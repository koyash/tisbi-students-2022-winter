﻿using System.Linq;
using System.Threading.Tasks;
using DAL3.Models.Book;

namespace MobileBackend.DI.BookRepo
{
    public class BookRepository
    {

        private ApplicationContext _applicationContext;

        public BookRepository(ApplicationContext _application)
        {
            _applicationContext = _application;
        }
        
        public Book FindBookByName(string searchName)
        {
            return _applicationContext.Book.FirstOrDefault(x => x.Name.Contains(searchName));
        }

        public async Task<bool> AddBook(Book newBook)
        {
            try
            {
                await _applicationContext.Book.AddAsync(newBook);
                await _applicationContext.SaveChangesAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}