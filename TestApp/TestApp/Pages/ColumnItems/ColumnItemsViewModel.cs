﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using Acr.UserDialogs;
using Xamarin.Forms;

namespace TestApp.Pages.ColumnItems
{
    public class ColumnItemsViewModel
    {
        public ObservableCollection<SomeClass> Items { get; set; } = new ObservableCollection<SomeClass>()
        {
            new SomeClass(){Desc = "desc 1", Name = "name 1"},
            new SomeClass(){Desc = "desc 2", Name = "name 2"},
            new SomeClass(){Desc = "desc 3", Name = "name 3"},
            new SomeClass(){Desc = "desc 4", Name = "name 4"},
            new SomeClass(){Desc = "desc 5", Name = "name 5"}
        };

        public ICommand SomeCommand => new Command<SomeClass>(async value =>
        {
            UserDialogs.Instance.Alert($"{value.Name} {value.Desc}", "Вы выбрали");
        });

    }
    
    public class SomeClass
    {
        public string Desc { get; set; }
        public string Name { get; set; }
    }
    
}