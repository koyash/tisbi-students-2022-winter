﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestApp.Pages.BookPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BookPage : ContentPage
    {
        private readonly BookViewModel _vm = new BookViewModel();
        public BookPage()
        {
            InitializeComponent();
            BindingContext = _vm;
        }
    }
}