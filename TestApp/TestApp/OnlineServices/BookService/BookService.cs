﻿using System.Threading.Tasks;
using DAL3.Models;
using DAL3.Models.Book;
using Refit;

namespace TestApp.OnlineServices.BookService
{
    public class BookService : BaseDataService<IBookService>, IBookService
    {
        public async Task<ApiResponse<Book>> FindBook(string bookName)
        {
            return await InstanceInterface.FindBook(bookName);
        }

        public async Task<ApiResponse<EmptyResponse>> AddBook(Book newBook)
        {
            return await InstanceInterface.AddBook(newBook);
        }
    }
}