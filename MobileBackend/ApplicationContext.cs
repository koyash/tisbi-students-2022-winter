﻿
using DAL3.Models.Book;
using Microsoft.EntityFrameworkCore;

namespace MobileBackend
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
            
        }
        
        public DbSet<Book> Book { get; set; }
        
    }
}