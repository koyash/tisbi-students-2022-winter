﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace TestApp.Converters
{
    public class IsTestPassedConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = (bool)value;
            return result
                ? "Тест пройден"
                : "Необходимо перепройти тест";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}