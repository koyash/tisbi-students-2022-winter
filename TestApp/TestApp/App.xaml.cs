﻿using TestApp.OnlineServices;
using TestApp.Pages.BookListPage;
using TestApp.Pages.BookPage;
using TestApp.Pages.ColumnItems;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

namespace TestApp
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainService.Init();
            
            MainPage = new NavigationPage(new BookListPage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

    }
}