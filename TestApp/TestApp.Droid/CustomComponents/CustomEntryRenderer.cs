﻿using Android.Content;
using TestApp.CustomComponents;
using TestApp.Droid.CustomComponents;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly:ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace TestApp.Droid.CustomComponents
{
    public class CustomEntryRenderer : EntryRenderer
    {
        
        public CustomEntryRenderer(Context context) : base(context)
        {}

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            Control.SetPadding(100, 0, 0, 0);
        }
    }
}