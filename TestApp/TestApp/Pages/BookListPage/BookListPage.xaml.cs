﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestApp.Pages.BookListPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BookListPage : ContentPage
    {
        private readonly BookListViewModel _vm = new BookListViewModel();
        public BookListPage()
        {
            InitializeComponent();
            BindingContext = _vm;
        }
    }
}