﻿using System.Threading.Tasks;
using DAL3.Models;
using DAL3.Models.Book;
using Refit;

namespace TestApp.OnlineServices.BookService
{
    public interface IBookService
    {

        [Get("/book/getBook?bookName={bookName}")]
        Task<ApiResponse<Book>> FindBook(string bookName);

        [Post("/book/addBook")]
        Task<ApiResponse<EmptyResponse>> AddBook(
            [Body]Book newBook);

    }
}