﻿using System;
using System.Globalization;
using DAL3.Models.Book;
using Xamarin.Forms;

namespace TestApp.Converters
{
    public class BookPageProgressBarConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var book = (Book)value;

            return book.CurrentPage / book.MaxPages;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}