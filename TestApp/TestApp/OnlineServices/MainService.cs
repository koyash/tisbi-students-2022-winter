﻿using TestApp.OnlineServices.BookService;

namespace TestApp.OnlineServices
{
    public static class MainService
    {
        public static void Init()
        {
            BookService = new BookService.BookService();
        }
        
        public static IBookService BookService { get; set; }
        
    }
}