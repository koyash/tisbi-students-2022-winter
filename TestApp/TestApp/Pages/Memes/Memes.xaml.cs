﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestApp.Pages.Memes
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Memes : ContentPage
    {
        private readonly MemesViewModel _vm = new MemesViewModel();
        public Memes()
        {
            InitializeComponent();
            BindingContext = _vm;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await _vm.UpdateData();
        }
    }
}