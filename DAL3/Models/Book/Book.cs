﻿namespace DAL3.Models.Book
{
    public class Book
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        public int CurrentPage { get; set; }
        public int MaxPages { get; set; }
    }
}