﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MobileBackend.DI.BookRepo;

namespace MobileBackend.Controllers.Book
{
    [ApiController]
    [Route("[controller]")]
    public class BookController : Controller
    {
        private readonly BookRepository _bookRepository;

        public BookController(BookRepository bookRepository)
        {
            this._bookRepository = bookRepository;
        }


        [HttpGet("getBook")]
        public ActionResult<DAL3.Models.Book.Book> GetBook(string bookName)
        {
            var foundBook = _bookRepository.FindBookByName(bookName);
            return foundBook != null
                ? Ok(foundBook)
                : BadRequest("Такой книги не найдено");
        }

        [HttpPost("addBook")]
        public async Task<ActionResult> AddBook(DAL3.Models.Book.Book newBook)
        {
            return await _bookRepository.AddBook(newBook) 
                ? Ok() 
                : BadRequest("Не удалось добавить книгу");
        }
        
    }
}