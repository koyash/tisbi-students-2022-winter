﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using DAL3.Models.Book;
using TestApp.Annotations;
using TestApp.OnlineServices;
using Xamarin.Forms;

namespace TestApp.Pages.BookPage
{
    public class BookViewModel : INotifyPropertyChanged
    {

        private Book _book;

        public Book BookItem
        {
            get => _book;
            set
            {
                _book = value;
                OnPropertyChanged();
            }
        }
        
        public ICommand AddBookCommand => new Command<string>(async newBookName =>
        {
            await MainService.BookService.AddBook(new Book() { Name = newBookName });
        });
        
        public ICommand LoadBookCommand => new Command<string>(async bookName =>
        {
            var response = await MainService.BookService.FindBook(bookName);
            if (response.IsSuccessStatusCode)
                BookItem = response.Content;
            else
                BookItem = new Book(){Name = "Такой книги не найдено"};
        });

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}