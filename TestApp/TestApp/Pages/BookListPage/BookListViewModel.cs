﻿using System.Collections.ObjectModel;
using DAL3.Models.Book;

namespace TestApp.Pages.BookListPage
{
    public class BookListViewModel
    {

        public ObservableCollection<Book> Books { get; set; } = new ObservableCollection<Book>()
        {
            new Book()
            {
                Name = "Book 1",
                Id = 1,
                MaxPages = 100,
                CurrentPage = 32
            },
            new Book()
            {
                Name = "Book 2",
                Id = 2,
                MaxPages = 120,
                CurrentPage = 76
            }
        };

    }
}