﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestApp.Pages.ColumnItems
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ColumnItems : ContentPage
    {

        private readonly ColumnItemsViewModel _vm = new ColumnItemsViewModel();
        
        public ColumnItems()
        {
            InitializeComponent();
            BindingContext = _vm;

            DrawCollection();
        }

        private void DrawCollection()
        {
            for (var i = 0; i < _vm.Items.Count; i+=2)
            {
                var stack = new StackLayout()
                {
                    Orientation = StackOrientation.Horizontal
                };


                var leftFrame = new Frame()
                {
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    BorderColor = Color.Black
                };
                var rightFrame = new Frame()
                {
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    BorderColor = Color.Black
                };

                var leftStack = new StackLayout()
                {
                    HorizontalOptions = LayoutOptions.FillAndExpand
                };
                var rightStack = new StackLayout()
                {
                    HorizontalOptions = LayoutOptions.FillAndExpand
                };
                
                var leftTitle = new Label()
                {
                    FontSize = 32,
                    Text = _vm.Items[i].Name,
                    HorizontalOptions = LayoutOptions.Center
                };

                var rightTitle = new Label()
                {
                    FontSize = 32,
                    HorizontalOptions = LayoutOptions.Center
                };

                var leftDescription = new Label()
                {
                    FontSize = 26,
                    Text = _vm.Items[i].Desc,
                    HorizontalOptions = LayoutOptions.Center
                };
                var rightDescription = new Label()
                {
                    FontSize = 26,
                    HorizontalOptions = LayoutOptions.Center
                };
                
                leftStack.Children.Add(leftTitle);
                leftStack.Children.Add(leftDescription);
                leftFrame.Content = leftStack;

                var leftTap = new TapGestureRecognizer
                {
                    Command = _vm.SomeCommand,
                    CommandParameter = _vm.Items[i]
                };

                leftFrame.GestureRecognizers.Add(leftTap);
                
                stack.Children.Add(leftFrame);

                if (i + 1 != _vm.Items.Count)
                {
                    rightTitle.Text = _vm.Items[i + 1].Name;
                    rightDescription.Text = _vm.Items[i + 1].Desc;
                    rightStack.Children.Add(rightTitle);
                    rightStack.Children.Add(rightDescription);

                    var rightTap = new TapGestureRecognizer()
                    {
                        Command = _vm.SomeCommand,
                        CommandParameter = _vm.Items[i + 1]
                    };
                    
                    rightFrame.Content = rightStack;
                    rightFrame.GestureRecognizers.Add(rightTap);
                }
                else
                {
                    rightFrame.BorderColor = Color.White;
                }
                
                stack.Children.Add(rightFrame);
                
                SomeStackLayout.Children.Add(stack);
            }
        }
    }
}