﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using DAL3.Models.Book;
using Newtonsoft.Json;
using TestApp.Annotations;
using Xamarin.Forms;

namespace TestApp.Pages.Memes
{
    public class MemesViewModel : INotifyPropertyChanged
    {
        private int _someInt;

        public int SomeInt
        {
            get => _someInt;
            set
            {
                _someInt = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<string> SomeCollection { get; set; }
            = new ObservableCollection<string>();


        public async Task UpdateData()
        {
            await Task.Delay(2000);
            int rnd = new Random().Next(1000);
            SomeInt = rnd;
        }

        public ICommand SomeCommand => new Command(async value => { await LoadData(); });

        private ObservableCollection<DAL3.Models.Book.Book> _books;

        public ObservableCollection<DAL3.Models.Book.Book> Books
        {
            get => _books;
            set
            {
                _books = value;
                OnPropertyChanged();
            }
        }

        public async Task LoadData()
        {
            var client = new HttpClient(GetInsecureHandler());
            var response = await client.GetAsync("http://10.0.2.2:5000/test/getBooks").ConfigureAwait(false);
            var json = await response.Content.ReadAsStringAsync();
            var books = JsonConvert.DeserializeObject<ObservableCollection<DAL3.Models.Book.Book>>(json);
            Books = books;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        
        
        public HttpClientHandler GetInsecureHandler()
        {
            HttpClientHandler handler = new HttpClientHandler();
            handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) =>
            {
                if (cert.Issuer.Equals("CN=localhost"))
                    return true;
                return errors == System.Net.Security.SslPolicyErrors.None;
            };
            return handler;
        }
        
    }
}